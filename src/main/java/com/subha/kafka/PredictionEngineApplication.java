package com.subha.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by subha on 08/07/2018.
 */

@SpringBootApplication
public class PredictionEngineApplication {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(PredictionEngineApplication.class, args);
        while (true) {
        }
    }
}
