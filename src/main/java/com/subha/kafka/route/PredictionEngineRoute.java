package com.subha.kafka.route;

import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by subha on 08/07/2018.
 */
@Component
public class PredictionEngineRoute extends RouteBuilder {


    @Autowired
    ConsumerTemplate consumer;



    @Override
    public void configure() throws Exception {
        errorHandler(deadLetterChannel("log:predictionEngine?level=ERROR&showHeaders=true&showBody=true&showCaughtException=true&showStackTrace=true")
                .useOriginalMessage());

        from("kafka:{{kafka.from-topic}}?brokers={{kafka.server}}:{{kafka.port}}&groupId={{kafka.channel}}_pe&autoOffsetReset=earliest&consumersCount=1")
                .routeId("prediction-engine")
                .to("bean:predictionEngine?method=predictNumbers")
                .to("kafka:{{kafka.to-topic}}?brokers={{kafka.server}}:{{kafka.port}}");
    }
}
