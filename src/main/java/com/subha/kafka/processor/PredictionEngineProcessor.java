package com.subha.kafka.processor;

import com.subha.kafka.domain.HistoricalGeneratedData;
import com.subha.kafka.repositories.GeneratedValueRepository;
import org.apache.camel.Consume;
import org.apache.camel.ConsumerTemplate;
import org.apache.camel.Exchange;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by subha on 08/07/2018.
 */

@Component("predictionEngine")
public class PredictionEngineProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(PredictionEngineProcessor.class);
    private Random random = new Random();
    private Integer seed;

    @Value("${kafka.generator-topic}")
    private String topic;

    @Value("${kafka.server}")
    private String server;

    @Value("${kafka.port}")
    private String port;

    @Value("${kafka.channel}")
    private String channel;

    @Value("${game.generator.number}")
    private String numberOfGenerators;

    @Value("${game.player.number}")
    private String numberOfPlayers;

    @Autowired
    private ConsumerTemplate consumerTemplate;

    public Integer getSeed() {
        return seed;
    }

    public void setSeed(Integer seed) {
        this.seed = seed;
    }

    @Value("${player.id}")
    private String playerId;

    private ReentrantLock lock = new ReentrantLock();

    @Autowired
    private GeneratedValueRepository repository;

    public GeneratedValueRepository getRepository() {
        return repository;
    }

    public void setRepository(GeneratedValueRepository repository) {
        this.repository = repository;
    }

    public void predictNumbers(Exchange exchange) {
        List<HistoricalGeneratedData> products = new ArrayList<>();

        setSeed(Integer.parseInt(exchange.getIn().getBody().toString().split("-")[0]));
        LOG.info("Got seed " + exchange.getIn().getBody().toString());
        repository.findAll().forEach(products::add);

        //if there is not enough historical data send random number
        if (products.size() <= 1) {
            exchange.getIn().setBody((random.nextInt(10000)+1) + "-" + playerId + "-" + exchange.getIn().getBody().toString().split("-")[1]);
        } else {
            SimpleRegression simpleRegression = new SimpleRegression(true);

            products.forEach(product -> {
                simpleRegression.addData(product.getSeed().doubleValue(), product.getGeneratedValue().doubleValue());
            });
            Double predictedValue = simpleRegression.predict(Double.parseDouble(exchange.getIn().getBody().toString().split("-")[0]));
            exchange.getIn().setBody(predictedValue.longValue() + "-" + playerId + "-" + exchange.getIn().getBody().toString().split("-")[1]);
        }
        LOG.info("Predicted number " + exchange.getIn().getBody().toString());
        insertHistoricData();
    }



    public void insertHistoricData() {
        for (int i = 0 ; i < Integer.parseInt(numberOfGenerators); i++){
            Exchange exchange = consumerTemplate.receive("kafka:" + topic + "?brokers=" + server + ":" + port + "&groupId=" + channel + "_data&autoOffsetReset=earliest&consumersCount=1");

                if(exchange.getIn().getBody().toString().contains("GEN5")) {
                    LOG.info("inerting generator data for five" + exchange.getIn().getBody());
                    HistoricalGeneratedData data = new HistoricalGeneratedData();
                    data.setSeed(getSeed());
                    data.setGeneratedValue(Long.valueOf(exchange.getIn().getBody().toString().split("-")[0]));
                    repository.save(data);
                }
            }

    }
}
