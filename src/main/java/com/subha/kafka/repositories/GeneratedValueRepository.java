package com.subha.kafka.repositories;

import com.subha.kafka.domain.HistoricalGeneratedData;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by subha on 08/07/2018.
 */
public interface GeneratedValueRepository extends CrudRepository<HistoricalGeneratedData, Long> {
}
