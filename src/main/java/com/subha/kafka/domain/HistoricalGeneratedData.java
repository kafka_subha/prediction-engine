package com.subha.kafka.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by subha on 08/07/2018.
 */

@Entity
@Table(name="hist_data")
public class HistoricalGeneratedData {

    @Id
    @GeneratedValue
    private Long id;

    private Integer seed;
    private Long generatedValue;


    public Integer getSeed() {
        return seed;
    }

    public void setSeed(Integer seed) {
        this.seed = seed;
    }

    public Long getGeneratedValue() {
        return generatedValue;
    }

    public void setGeneratedValue(Long generatedValue) {
        this.generatedValue = generatedValue;
    }
}
