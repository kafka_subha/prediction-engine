****Prediction Engine Application****

This application will receive a seed from game controller via a topic , 
and will try to predict the generated number by generator five. 
After prediction it will capture the result from generator.

Topic configuration are defined in application.yml file.